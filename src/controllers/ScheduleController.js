const ScheduleService = require('../services/ScheduleService')

const list = async (req, res) => {
    try {
        const schedules = await ScheduleService.list(req.body)
        res.send(schedules)
    } catch (error) {
        res.status(400).send(error)
    }
}

module.exports = {list}