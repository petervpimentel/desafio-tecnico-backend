const ScheduleRuleService = require('../services/ScheduleRuleService')

const list = async (req, res) => {
    try {
        const rules = await ScheduleRuleService.list()
        res.send(rules)
    } catch (error) {
        res.status(400).send(error)
    }
}

const save = async (req, res) => {
    try {
        const rulesSaved = await ScheduleRuleService.save(req.body)
        res.send(rulesSaved)
    } catch (error) {
        res.status(400).send(error)
    }
}

const remove = async (req, res) => {
    try {
        const message = await ScheduleRuleService.remove(req.params.id)
        res.send(message)
    } catch (error) {
        res.status(400).send(error)
    }
}

module.exports = {list, save, remove}