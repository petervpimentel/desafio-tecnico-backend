const ScheduleRuleService = require('../services/ScheduleRuleService')

const validateInsert = (req, res, next) => {
    try {
        ScheduleRuleService.validateInsert(req.body)
        next();        
    } catch (error) {
        res.status(400).send(error)
    }
}

module.exports = {validateInsert}