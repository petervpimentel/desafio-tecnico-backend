const fs = jest.genMockFromModule('fs');

const rules = require('./rules.json')

let ruleList = '[]';

//Função customizada que o teste pode utilizar
//para definir quais valores serão retornados pelo readFile
function __setMockFiles(type) {
    switch (type) {
        case 1:
            ruleList = '[]'
            break;
        case 2:
            ruleList = JSON.stringify(rules.RULES_2)
            break;
        case 3:
            ruleList = JSON.stringify(rules.RULES_3)
            break;
        case 4:
            ruleList = JSON.stringify(rules.RULES_4)
            break;
    }
}

//Versão customizada do `readFile` onde os dados vem do __setMockFiles
async function readFile (directoryPath,cb) {
    return cb(null, ruleList);
}

async function writeFile(directoryPath, file, cb) {
    return cb(null)
}

fs.__setMockFiles = __setMockFiles;
fs.readFile = readFile;
fs.writeFile = writeFile;

module.exports = fs;