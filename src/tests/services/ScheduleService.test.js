const ScheduleService = require('../../services/ScheduleService')

const messages = require('../../constants/messages.json')

const mockRules = require('../__mocks__/rules.json')
const path = require('path');

jest.mock('fs');

const fs = require('fs')

describe('Listagem de horários com parâmetros de filtro inválidos',()=>{
    
    test('Data fim menor que data inicio', async (done) => {
        expect.assertions(1);
        fs.__setMockFiles(1);
        try {
            await ScheduleService.list({
                "dateInit":"25-06-2019",
                "dateEnd":"24-06-2019"
            })
        } catch (error) {
            expect(error).toMatchObject({message:messages.DATA_INVALIDA})
        }
        done()
    });

    test('Data em formato inválido', async (done) => {
        expect.assertions(1);
        fs.__setMockFiles(1);
        try {
            await ScheduleService.list({
                "dateInit":"25-06-2019",
                "dateEnd":"24/06/2019"
            })
        } catch (error) {
            expect(error).toMatchObject({message:messages.DATA_INVALIDA})
        }
        done()
    });

    test('Data FIM não informada', async (done) => {
        expect.assertions(1);
        fs.__setMockFiles(1);
        try {
            await ScheduleService.list({
                "dateInit":"25-06-2019"
            })
        } catch (error) {
            expect(error).toMatchObject({message:messages.SEM_DATA_FIM})
        }
        done()
    });

    test('Data INIT não informada', async (done) => {
        expect.assertions(1);
        fs.__setMockFiles(1);
        try {
            await ScheduleService.list({
                "dateEnd":"25-06-2019"
            })
        } catch (error) {
            expect(error).toMatchObject({message:messages.SEM_DATA_INIT})
        }
        done()
    });
})