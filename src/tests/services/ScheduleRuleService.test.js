const ScheduleRuleService = require('../../services/ScheduleRuleService')
const messages = require('../../constants/messages.json')

const mockRules = require('../__mocks__/rules.json')
const path = require('path');

jest.mock('fs');

const fs = require('fs')

describe('Testando a listagem de regras',()=>{
    
    test('Deve retornar mensagem que não existe regras', async (done) => {
        expect.assertions(1);
        fs.__setMockFiles(1);
        try {
            const msg = await ScheduleRuleService.list()
            expect(msg).toBe(messages.SEM_REGRAS)
        } catch (error) {
        }
        done()
    });
    
    test('Deve retornar a lista de regras', async (done) => {
        expect.assertions(1);
        fs.__setMockFiles(2);
        try {
            const rules = await ScheduleRuleService.list()
            expect(rules).toMatchObject(mockRules.RULES_2)
        } catch (error) {
        }
        done()
    });
})

describe('Testando a remoção de regras',()=>{
    
    test('Deve retornar a mensagem com o ID da regra removida', async (done) => {
        fs.__setMockFiles(4);
        try {
            const msg = await ScheduleRuleService.remove(mockRules.RULES_4[0].id)

            expect(msg).toMatchObject({message: `A regra de ID: ${mockRules.RULES_4[0].id} foi removida com sucesso.`})
        } catch (error) {
        }
        done()
    });
})