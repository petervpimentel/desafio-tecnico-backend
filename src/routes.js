const express = require('express');
const ScheduleRuleController = require('./controllers/ScheduleRuleController');
const ScheduleRuleValidator = require('./controllers/ScheduleRuleValidator');
const ScheduleController = require('./controllers/ScheduleController');

const routes = new express.Router();

const SCHEDULE_BASE_URL =  "/api/schedule";

routes.post(`${SCHEDULE_BASE_URL}`, ScheduleController.list);

routes.get(`${SCHEDULE_BASE_URL}/rules`, ScheduleRuleController.list);
routes.post(`${SCHEDULE_BASE_URL}/rules`, ScheduleRuleValidator.validateInsert, ScheduleRuleController.save);
routes.delete(`${SCHEDULE_BASE_URL}/rules/:id`, ScheduleRuleController.remove);


module.exports = routes;
