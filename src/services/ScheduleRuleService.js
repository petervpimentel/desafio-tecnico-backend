const util = require('util');
const path = require('path');
const fs = require('fs');
const nanoid = require('nanoid');
const isValid = require('date-fns/is_valid');

const messages = require('../constants/messages.json');
const rulesTypes = require('../constants/rulesType.json');

const readFile = util.promisify(fs.readFile);
const writeFile = util.promisify(fs.writeFile);

const _saveOnFile =  async (data) => {
    await writeFile(path.resolve(__dirname,"..","..","src/Data","data.json"), JSON.stringify(data))
}

const _listFromFile = async () => {
    try {
        const rawData = await readFile(path.resolve(__dirname,"..","..","src/Data","data.json"));

        //Verificar se o arquivos está vazio e prevenir o erro do JSON.parse no arquivo vazio
        if(rawData.length === 0)
            return []

        return JSON.parse(rawData)
    } catch (error) {
        if(error.code === "ENOENT"){
            return []
        }else{
            throw {message: messages.ERRO_LIST_REG}
        }
    }
}

const list = async () => {
    try {
        const rules = await _listFromFile()

        if(rules.length < 1)
            return {message: messages.SEM_REGRAS}

        return rules
    } catch (error) {
        throw error
    }
}

const save = async (rules) => {
    try {
        const normalizedRules = rules.map(rule => ( {...rule,id:nanoid()} ) );
        
        const rawRules = await _listFromFile();
                
        const rulesToSave = rawRules.concat(...normalizedRules);
        
        await _saveOnFile(rulesToSave)
        
        return normalizedRules
    } catch (error) {
        throw {message: error.message || messages.ERRO_CADASTRO_REG}
    }
}

const remove =  async (id) => {
    try {
        const rawRules = await _listFromFile();

        const rulesToSave = rawRules.filter(rule => rule.id !== id)
        
        await _saveOnFile(rulesToSave)

        return {message: `A regra de ID: ${id} foi removida com sucesso.`}

    } catch (error) {
        throw {message: messages.ERRO_REMOVER_REG}
    }
}

const validateInsert = (rules) => {
    if(!Array.isArray(rules))
        throw {message: messages.REGRA_SEM_ARRAY}
    
    rules.forEach(rule => {
        //Verificar se foi informado um período para a regra
        if(!rule.day)
            throw {message: messages.REGRA_SEM_DATA}
        if(rule.day === rulesTypes.DAILY){
            _validateIntervals(rule)
        }else if(rulesTypes.WEEKDAYS.some(weekDay => rule.day === weekDay)){
            _validateIntervals(rule)
        }else{
            const dateSplit = rule.day.split("-")
            
            //Se data tem o formato correto
            if(dateSplit.length !== 3)
                throw {message: messages.DATA_INVALIDA}
            
            const date = new Date(dateSplit[2],dateSplit[1],dateSplit[0]);
            
            //Se é uma data válida
            if(!isValid(date))
                throw {message: messages.DATA_INVALIDA}
            
            _validateIntervals(rule)
        }
    })

    return true
}

const _validateIntervals = (rule) => {
    if(!rule.intervals)
        throw {message: messages.REGRA_SEM_INTERVALO}
    
    if(!Array.isArray(rule.intervals))
        throw {message: messages.REGRA_SEM_INTERVALO}
    
    if(rule.intervals.length < 1)
        throw {message: messages.REGRA_SEM_INTERVALO}

    rule.intervals.forEach(inter => {
        if(!inter.start || !inter.start)
            throw {message: messages.REGRA_SEM_INTERVALO}
    })

    return true
}

module.exports = { list, save, remove, validateInsert }
