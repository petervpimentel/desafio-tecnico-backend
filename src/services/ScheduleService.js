const ScheduleRuleService = require('./ScheduleRuleService');
const isValid = require('date-fns/is_valid');
const isAfter = require('date-fns/is_after');
const isEqual = require('date-fns/is_equal');
const eachDay = require('date-fns/each_day');
const dateFormat = require('date-fns/format');
const isWithinRange = require('date-fns/is_within_range');

const rulesTypes = require('../constants/rulesType.json');
const messages = require('../constants/messages.json');

const _extractRulesbyDates = (rules, dateInit, dateEnd) => {

    return rules.filter(rule => {
        if(rule.day === rulesTypes.DAILY){
            return true
        }else if(rulesTypes.WEEKDAYS.some(weekDay => rule.day === weekDay)){
            return true
        }else {
            const date = rule.day.split("-")
            return isWithinRange(
                new Date(date[2],date[1],date[0]),
                new Date(dateInit[2],dateInit[1],dateInit[0]),
                new Date(dateEnd[2],dateEnd[1],dateEnd[0])
            )
        }
    });
}

const _validateFilters = (dateInit, dateEnd) => {
    if(dateInit.length === 3 && dateEnd.length === 3){
        const init =  new Date(dateInit[2],dateInit[1],dateInit[0]);
        const end = new Date(dateEnd[2],dateEnd[1],dateEnd[0]);
        
        return (dateInit[1] > 0
            && dateEnd[1] > 0
            && isValid(init)
            && isValid(end)
            && ( isAfter(end, init) || isEqual(end, init))
        );
    }else{
        return false
    }
}

const _format = (rules, dateInit, dateEnd) => {
    //Criar um array com as datas que estão no range selecionado
    const days = eachDay(
        new Date(dateInit[2],parseInt(dateInit[1])-1,dateInit[0]),
        new Date(dateEnd[2],parseInt(dateEnd[1])-1,dateEnd[0])
    )

    //Percorrer as datas para inserir os dados retornando um array de mesmo tamanho
    const schedules = days.map(day => {
        const formatedDay = dateFormat(day,'DD-MM-YYYY')
        
        const schedule = {
            day: formatedDay,
            intervals:[]
        }
        
        //Inserir em cada data as regras que são compativeis
        rules.forEach(rule => {
            if(rule.day === rulesTypes.DAILY){
                schedule.intervals.push(...rule.intervals)
            }else if(formatedDay === rule.day){
                schedule.intervals.push(...rule.intervals);
            }else if(rule.day === rulesTypes.WEEKDAYS[day.getDay()]) {
                schedule.intervals.push(...rule.intervals);
            }
        });
        return schedule;
    });
    //removendo da lista as datas sem regras
    return schedules.filter(schedule => schedule.intervals.length > 0);
}

const list = async (filters) => {
    try {
        if(!filters.dateInit)
            throw {message:messages.SEM_DATA_INIT}
        if(!filters.dateEnd)
            throw {message:messages.SEM_DATA_FIM}
        
        const dateInit =  filters.dateInit.split("-");
        const dateEnd =  filters.dateEnd.split("-");

        if(!_validateFilters(dateInit, dateEnd))
            throw {message:messages.DATA_INVALIDA}
        
        //Buscar todas as regras cadastradas
        const rules = await ScheduleRuleService.list();

        if(rules.message)
            throw rules

        //Trazer somente as datas que estão no range do filtro
        const validRules = _extractRulesbyDates(rules, dateInit, dateEnd);

        //Juntar as regras de mesma data/diarias/semanais com IDs diferentes
        const formatedRules = _format(validRules, dateInit, dateEnd)

        return formatedRules
    } catch (error) {
        throw {message: error.message || messages.ERRO_LIST_HORARIOS}
    }
}

module.exports = {list}