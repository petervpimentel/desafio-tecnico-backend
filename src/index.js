const express = require('express');
const bodyParser = require('body-parser');

const app = express();

const PORT = 3333

app.use(bodyParser.json());
app.use(require('./routes.js'));

app.listen(PORT, () => {
  console.log(`App listening on port ${PORT}`);
});
