<h2 align="center">
  Desafio Backend
</h2>

----------------

<p align="center">
  API REST para facilitar o gerenciamento de horários de uma clínica
</p>

[Desafio](https://git.cubos.io/cubos/desafio-tecnico-backend)


|         | Main Features  |
----------|-----------------
➕ | Inserir regras de hórarios  `POST - /api/schedule/rules`
📜 | Listar regras existentes`GET - /api/schedule/rules`
❌ | Remover regra `DELETE - /api/schedule/rules/id_regra`
⏰ | Listar horarios disponíveis `POST - /api/scheduleid_regra`


##### Requirements
- Node
- npm

#### Getting Started

```bash
$ git clone git@gitlab.com:petervpimentel/desafio-tecnico-backend.git
$ cd desafio-tecnico-backend
$ npm install
$ npm start
```

#### Criando um regra
Para criação de uma regra é necessário que a regra tenha o seguinte formato:

```
[
  {
    "day":"periodo_da_regra",
    "intervals": [
      { "start": "horario_inicio", "end": "horario_fim" }
    ]
  },
  {
    "day":"periodo_da_regra",
    "intervals": [
      { "start": "horario_inicio", "end": "horario_fim" }
    ]
  }
]
```

As regras podem ser inseridas em lotes, assim como os intervalos em que ela será aplicada.

##### Regra com data fixa

----------------

Para a criação de uma regra com a data fixa, siga o seguinte formato:
```
{
  "day":"30-06-2019",
  "intervals": [
    { "start": "01:00", "end": "04:00" }
  ]
}
```
- No campo `day`passar o valor da data desejada no formato `DD-MM-YYY`.
- O campo `intervals` segue o formato padrão das outras regras.

##### Regra diária

----------------

Para a criação de uma regra diária é necessário que a regra siga o seguinte formato:
```
{
  "day":"daily",
  "intervals": [
    { "start": "01:00", "end": "04:00" }
  ]
}
```
- No campo `day`passar o valor `daily`.
- O campo `intervals` segue o formato padrão das outras regras.

##### Regra semanal

----------------

Para a criação de uma regra semanal é necessário que a regra siga o seguinte formato:
```
{
  "day":"sunday",
  "intervals": [
    { "start": "01:00", "end": "04:00" }
  ]
}
```
- No campo `day`passar o dia da semana ex:`sunday` ou `tuesday`.
- O campo `intervals` segue o formato padrão das outras regras.